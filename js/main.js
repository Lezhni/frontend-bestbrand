var hwSlideSpeed = 700;
var hwTimeOut = 3000;
var hwNeedLinks = true;

$(document).ready(function() {

    $('.dotdotdot').dotdotdot();

    $('.slide').css(
        {"position" : "absolute",
            "top":'0', "left": '0'}).hide().eq(0).show();
    var slideNum = 0;
    var slideTime;
    slideCount = $(".slider .slide").size();
    var animSlide = function(arrow){
        clearTimeout(slideTime);
        $('.slide').eq(slideNum).fadeOut(hwSlideSpeed);
        if(arrow == "next"){
            if(slideNum == (slideCount-1)){slideNum=0;}
            else{slideNum++}
        }
        else if(arrow == "prew")
        {
            if(slideNum == 0){slideNum=slideCount-1;}
            else{slideNum-=1}
        }
        else{
            slideNum = arrow;
        }
        $('.slide').eq(slideNum).fadeIn(hwSlideSpeed, rotator);
        $(".nav.active").removeClass("active");
        $('.nav').eq(slideNum).addClass('active');
    }
    if(hwNeedLinks){
        $('.next').click(function(){
            animSlide("next");
            return false;
        })
        $('.prev').click(function(){
            animSlide("prew")
            return false;
        })
    }
    var pause = false;
    var rotator = function(){
        if(!pause){slideTime = setTimeout(function(){animSlide('next')}, hwTimeOut);}
    }
    $('.slider').hover(
        function(){clearTimeout(slideTime); pause = true;},
        function(){pause = false; rotator();
        });
    rotator();

    var winners = $('.winner-single').size();
    var block_h = 168;
    var offset = 0;
    var counter = 2;

    $('.down').click(function() {
        offset = parseInt($('.winners-inner').css('top'));
        if (isNaN(offset)) {
            offset = 0;
        }
        console.log(counter);
        if (counter < winners) {
            offset = offset - block_h;
           $('.winners-inner').animate({'top': offset + 'px'}, 300);
           counter++;
        }
    });
    $('.up').click(function() {
        offset = parseInt($('.winners-inner').css('top'));
        if (isNaN(offset)) {
            offset = 0;
        }
        console.log(counter);
        if (counter >= winners) {
            offset = offset + block_h;
           $('.winners-inner').animate({'top': offset + 'px'}, 300);
            if (counter > 2) {
                counter--;
            }
        }
    });

    $('.partners-slider').owlCarousel({
        items: 7,
        autoPlay: 3000,
        navigation: true
    });

    $('.to-top').click(function() {
        $('body, html').animate({scrollTop: 0}, 200);
    })
});